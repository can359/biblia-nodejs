const mongoose = require("mongoose");
const { MONGO_URI } = require("./config");
const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};
const axios = require("axios").default;
const cheerio = require("cheerio");
const cron = require("node-cron");

mongoose.connect(MONGO_URI, options).then(
  () => { console.log("Connected to mongoDB"); },
  err => { console.log(err) }
);
const { BreakingNew } = require("./models");

cron.schedule("0 */4 * * *", async () => {
  console.log("Cron executed");
  const html = await axios.get("https://cnnespanol.cnn.com/");
  const $ = cheerio.load(html.data);
  const titles = $(".news__title");
  const breakingNews = [];
  titles.each((index, element) => {
    const breakingNew = {
      title: $(element).children().attr("title"),
      link: $(element).children().attr("href"),
    };
    breakingNews.push(breakingNew);
  });
  BreakingNew.create(breakingNews);
});
