module.exports = {
  HomeRoutes: require("./home.routes"),
  UserRoutes: require("./user.routes"),
  IdeaRoutes: require("./ideas.routes"),
  CommentRoutes: require("./commet.routes"),
  AuthRoutes: require("./auth.routes"),
};
