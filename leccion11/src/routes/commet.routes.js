const { Router } = require("express");
const { AuthMiddleware } = require("../middlewares");

module.exports = function ({ CommentController }) {
  const router = Router();

  router.get("/:ideaId", AuthMiddleware, CommentController.getIdeaComments);
  router.get("/:commentId/unique", AuthMiddleware, CommentController.get);
  router.post("/:ideaId", AuthMiddleware, CommentController.create);
  router.put("/:commentId", AuthMiddleware, CommentController.update);
  router.delete("/:commentId", AuthMiddleware, CommentController.delete);

  return router;
};
