const { IdeaRepository } = require("../../../src/repositories");
const mockingoose = require("mockingoose");
const { Idea } = require("../../../src/models");
let {
  IdeaModelMock: { idea, ideas },
} = require("../../mocks");

describe("Ideas Repository Tests", () => {
  beforeEach(() => {
    mockingoose.resetAll();
    jest.clearAllMocks();
  });

  it("Should return an idea collection", async () => {
    const _ideas = ideas.map((idea) => idea);

    mockingoose(Idea).toReturn(ideas, "find");

    const _ideaRepository = new IdeaRepository({ Idea });
    const expected = await _ideaRepository.getAll();

    expect(JSON.parse(JSON.stringify(expected))).toMatchObject(_ideas);
  });

  it("Should return an idea by id", async () => {
    const _idea = { ...idea };

    mockingoose(Idea).toReturn(idea, "findOne");

    const _ideaRepository = new IdeaRepository({ Idea });
    const expected = await _ideaRepository.get(_idea._id);

    expect(JSON.parse(JSON.stringify(expected))).toMatchObject(idea);
  });
});
