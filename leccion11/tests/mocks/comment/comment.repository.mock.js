module.exports = {
  get: jest.fn(),
  create: jest.fn(),
  update: jest.fn(),
  delete: jest.fn(),
  getIdeaComments: jest.fn(),
};
