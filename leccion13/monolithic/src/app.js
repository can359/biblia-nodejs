const express = require("express");
const app = express();

const response = {
  data: [],
  services: "Monolothic service",
  architecture: "Monolithic",
};

app.use((req, res, next) => {
  response.data = [];
  next();
});

app.get("/api/v1/users", (req, res) => {
  response.data.push("Lisa", "Bart", "Maggie");
  return res.send(response);
});

app.get("/api/v1/books", (req, res) => {
  response.data.push(
    "The clean coder",
    "The programatic programmer",
    "Soft skils"
  );
  return res.send(response);
});

app.get("/api/v1/cars", (req, res) => {
  response.data.push("Fiat", "Pontiac", "Jeep");
  return res.send(response);
});

module.exports = app;
