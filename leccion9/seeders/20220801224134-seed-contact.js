'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert("Contacts", [
      {
        firstname: "Lisa",
        lastname: "Simpson",
        phone: "999999999",
        email: "lsimpson@gmail.com",
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString()
      },
      {
        firstname: "Marge",
        lastname: "Simpson",
        phone: "888888888",
        email: "msimpson@gmail.com",
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString()
      }
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete("Contacts", null, {});
  }
};
