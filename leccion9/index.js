const { CRUD } = require("./helpers");
const db = require("./models");

const params = process.argv;

if (params.length <= 2) {
  process.exit(0);
}

const args = params.slice(2);

const command = args[0].split(":")[0].substring(2);
const entity = args[0].split(":")[1];

switch (command) {
  case CRUD.CREATE:
    const data = {};
    args.slice(1).map(arg => {
      const tmp = arg.split("=");
      data[tmp[0].substring(2)] = tmp[1];
    });
    db[entity]
      .create(data)
      .then(() => console.log("Contact created"))
      .catch(console.log)
    break;

  case CRUD.READ:
    db[entity]
      .findAll()
      .then(console.log)
      .catch(console.log)
    break;

  case CRUD.UPDATE:
    let id = 0;
    let up = {};
    args.slice(1).map(arg => {
      let temp = arg.split("=");
      if (temp[0].slice(2) === "id") {
        id = temp[1];
      }
      else {
        up[temp[0].slice(2)] = temp[1];
      }
    });
    db[entity]
      .update(up, {
        where: {
          id
        }
      })
      .then(() => console.log(`Contact ${id} updated`))
      .catch(console.log)
    break;

  case CRUD.DELETE:
    let idx = 0;
    args.slice(1).map(arg => {
      let tmp = arg.split("=");
      idx = tmp[1];
    });
    db[entity]
      .destroy({
        where: {
          id: idx
        },
      })
      .then(() => console.log(`Contact ${idx} deleted`))
      .catch(console.log);
    break;

  default:
    console.log("Operation not found");
    break;
}
